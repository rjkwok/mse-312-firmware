#ifndef MSE312_TIMESERIES
#define MSE312_TIMESERIES

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#elif defined(WIRING)
#include "Wiring.h"
#else
#include "WProgram.h"
#include "pins_arduino.h"
#endif

#define N 2500

#include "utility/direct_pin_read.h"

class Timeseries {

    int16_t samples[N] = {0};

public:

    // pops a sample of the back and adds a new value to the front
    void add_sample(int16_t x) {
      for(uint16_t i = 1; i < N; i++) {
        samples[i] = samples[i - 1];
      }
      samples[0] = x;
    }

    int16_t get_current_position() {
      return samples[0];
    }

    int16_t get_absolute_error(int16_t target) {
      return abs(target - get_current_position());
    }
    
    // calculate the maximum range of the stored samples
    int16_t get_peak_to_peak_ripple() {
      int16_t max_sample = samples[0];
      int16_t min_sample = samples[0];
      for(int16_t i = 1; i < N; i++) {
        if(samples[i] > max_sample) {
          max_sample = samples[i];
        }
        if(samples[i] < min_sample) {
          min_sample = samples[i];
        }
      }
      return max_sample - min_sample;
    }
};

#endif
