#ifndef MSE_TIMER
#define MSE_TIMER

#if defined(ARDUINO) && ARDUINO >= 100
#include "Arduino.h"
#elif defined(WIRING)
#include "Wiring.h"
#else
#include "WProgram.h"
#include "pins_arduino.h"
#endif


class MSEtimer {
    uint32_t timer_start_time = 0;
    uint32_t delta = 0;
    boolean increment = false;

public:

    /* Reset time
     *    - set this timer's millis offset to 0
     */
    void reset_time(void){
      timer_start_time = millis();
      increment = false;
      delta = 0;
    }

    /* Start time
     *    - allows the timer to count again from its previous value
     */
    void start_time(void){
      increment = true;
    }


    /* Is_over
     *    - returns true if it is [val] ms more than start time
     */
    boolean is_over(uint32_t val){
      if(increment){
        return (millis() - val) > timer_start_time;
      }
      return false;
    }


    /* Get delta
     *    - returns ms difference between start time and now
     */
    uint32_t get_delta(void){
      if(increment){
        delta = millis() - timer_start_time;
      }
      return delta;
    }

    /* Stop time
     *    - stops this timer and returns the total elapsed time in ms
     */
     uint32_t stop_time(void){
      if(increment){
        increment = 0;
        return millis() - timer_start_time;
      }
      else return 0;
     }
};

#endif
