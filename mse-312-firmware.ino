#include "Encoder.h"
#include "PID_v1.h"
#include "Timeseries.h"
#include "Timer.h"
#include "MSEtimer.h"

#define MANUAL 0    // 1 = testing over serial terminal, 0 = GUI

/* Commands:
 *    s = stop immediately
 *    g = trigger a run
 *    e = on/off trigger for printing encoder values for debugging
 *    v = toggle verbose mode
 *    q = print PID values
 *    z = zero the encoder count
 *    
 * PID Tuning Commands:
 *    p = set the changable PID constant to kp
 *    i = set the changable PID constant to ki
 *    d = set the changable PID constant to kd
 *    r = set ripple 
 *    x = set error
 *    
 *    j = reduce currently selected constant
 *    k = increase currently selected constant
 *    m = toggle magnet
 * 
 */

// PID Setup
double Setpoint, Input, Output;
double kp, ki, kd;
PID myPID(&Input, &Output, &Setpoint, kp, ki, kd, DIRECT);

Encoder encoder(3, 18);
Timeseries encoder_data;
Timer timer;

MSEtimer run_timer;



// Command System Support
String input_string = "";         // String to hold incoming commands
volatile double change_err;       // Slight hack: these are used by the gain tuning system since these are a different type than the PID constants
volatile double change_ripple;
double* pid_to_change = &kp;      // Pointer to the current P/I/D/other value to tweak with up/down commands

// Pin settings
int MAGNET_PIN = 6; // PWM capable
int MOTOR_CW_PIN = 5; // PWM capable
int MOTOR_CCW_PIN = 4;  // PWM capable
int LED_PIN = 13; // For debugging
int LS1 = 19; // interrupt capable
int LS2 = 21; // interrupt capable
int LS3 = 22; // interrupt capable

// Program constants and types
enum ProgramState {STATE_IDLE, STATE_MOVETO_PICKUP, STATE_MOVETO_DROP, STATE_MOVETO_INITIAL};
enum MagnetState {OFF, ON};
double INITIAL_POSITION = 0;
double PICKUP_POSITION = 180;
double DROP_POSITION = 90;

// Tolerances are absolute value
uint16_t ERROR_TOLERANCE = 3;   // Accuracy
uint16_t RIPPLE_TOLERANCE = 3;  // Precision

// Program state variables
ProgramState state;
boolean print_verbose = MANUAL;
MagnetState magnet_state;
uint32_t run_time_ms;


// SETUP FUNCTION - Runs once
void setup() {
  state = STATE_IDLE;
  pinMode(MAGNET_PIN,   OUTPUT);
  pinMode(MOTOR_CW_PIN,  OUTPUT);
  pinMode(MOTOR_CCW_PIN, OUTPUT);
  pinMode(LED_PIN, OUTPUT); 

  // Interrupt pin setup
  pinMode(LS1, INPUT_PULLUP); // limit switch common = GND
  pinMode(LS2, INPUT_PULLUP); // limit switch common = GND
  pinMode(LS3, INPUT_PULLUP); // limit switch common = GND


  Serial.begin(115200);
  input_string.reserve(200);

  // Initialize PID
  Setpoint = 0;
  myPID.SetMode(AUTOMATIC);
  myPID.SetOutputLimits(-100, 100);
  
  kp = 0.0001;//0.025;//0.211;
  ki = 0.240;//0.246;//0.250; 
  kd = 0.294;//0.304;//0.29;
  myPID.SetTunings(kp, ki, kd);

  set_magnet_state(OFF);

  // Set up timer callbacks
  #if MANUAL
    timer.every(1000, send_state);  // Sending state variables demo in manual mode
  #else 
    timer.every(50, send_state);    // Increased frequency when using GUI
  #endif

  timer.every(2, add_sample);       // Add sample to stability checker
}


// LOOP - RUNS REPEATEDLY
void loop() {

  // Simple case-based state machine
  switch (state) {
    case STATE_IDLE: 
      // Idle: zero the motor inputs, arm will be loose 
      zero_voltages();
      run_timer.stop_time();
      break;
      
    case STATE_MOVETO_PICKUP:
      if (target_reached(int(PICKUP_POSITION))) {
        set_magnet_state(ON);
        hold_position(PICKUP_POSITION, 2500);
        state = STATE_MOVETO_DROP;
      }
      else {
        move_to(PICKUP_POSITION);
      }
      break;
    case STATE_MOVETO_DROP:
      if (target_reached(DROP_POSITION)) {
        hold_position(DROP_POSITION, 1500);
        set_magnet_state(OFF);
        hold_position(DROP_POSITION, 1500);
        state = STATE_MOVETO_INITIAL;
      }
      else {
        move_to(DROP_POSITION);
      }
      break;
    case STATE_MOVETO_INITIAL:
      if (target_reached(INITIAL_POSITION)) {
        state = STATE_IDLE;
      }
      else {
        move_to(INITIAL_POSITION);
      }
      break;
    default:
      // not a valid state, turn things off and revert to IDLE
      // TODO: state transition function that adds safety calls to all transitions
      set_magnet_state(OFF);
      state = STATE_IDLE;
      break;
  }
  timer.update();

  // Poll the limit switches
  if(digitalRead(LS1) == HIGH) limit_switch_1();
  if(digitalRead(LS2) == HIGH) limit_switch_2();
  if(digitalRead(LS3) == HIGH) limit_switch_3();
}
/* Encoder 
 *  - reads current encoder position in degrees
 *  uint16_t initial: initial position to subtract from current reading
 *  
 *  Example: current = read_encoder(0);
 */
int16_t read_encoder(uint16_t initial){
  int32_t pos = encoder.read();  // Conversion to degrees
  pos = pos / (4 * 500 * 6.3) * 360;
  return pos - initial;
}

/*
  serial_event occurs whenever a new data comes in the
  hardware serial RX.  This routine is run between each
  time loop() runs, so using delay inside loop can delay
  response.  Multiple bytes of data may be available.
*/
void serialEvent() {
  while (Serial.available()) {
    char input_char = (char)Serial.read();
    input_string += input_char;
    if (input_char == '\n') {
      // Parse for state changing
      if (input_string[0] == 's'){
        state = STATE_IDLE;
        print_verbose = false;
        run_timer.reset_time();
        run_timer.start_time();
      }
      else if (input_string[0] == 'g'){
        state = STATE_MOVETO_PICKUP;
        run_timer.reset_time();
        run_timer.start_time();
      }
      else if (input_string[0] == 'j' || input_string[0] == 'k') change_constant(input_string[0]);
      else if (input_string[0] == 'p') pid_to_change = &kp;
      else if (input_string[0] == 'i') pid_to_change = &ki;
      else if (input_string[0] == 'd') pid_to_change = &kd;
      else if (input_string[0] == 'r') pid_to_change = &change_ripple;
      else if (input_string[0] == 'x') pid_to_change = &change_err;
      else if (input_string[0] == 'q') print_pid();
      else if (input_string[0] == 'v') print_verbose = !print_verbose;
      else if (input_string[0] == 'm') (magnet_state == OFF) ? set_magnet_state(ON) : set_magnet_state(OFF);
      else if (input_string[0] == 'z'){
        encoder.write(0);   // Zero the encoder count
        // Hack to get the accumulated PID integral and derivative to be zeroed
        Input = 0.0; 
        myPID.SetOutputLimits(0.0, 1.0);  // Forces minimum up to 0.0
        myPID.SetOutputLimits(-1.0, 0.0);  // Forces maximum down to 0.0
        myPID.SetOutputLimits(-100, 100);  // Set the limits back to normal
      }

      clear_string();
    }
  }
}

/* Change constant
 *    - updates the currently selected constant
 */
void change_constant(char input){
  if(pid_to_change == &change_err){
    if(input == 'k') ERROR_TOLERANCE = ERROR_TOLERANCE + 1;
    else if(input == 'j' && ERROR_TOLERANCE > 0) ERROR_TOLERANCE = ERROR_TOLERANCE - 1;
  }
  else if(pid_to_change == &change_ripple){
    if(input == 'k') RIPPLE_TOLERANCE = RIPPLE_TOLERANCE + 1;
    else if(input == 'j' && RIPPLE_TOLERANCE > 0) RIPPLE_TOLERANCE = RIPPLE_TOLERANCE - 1;
  }
  else if(pid_to_change == &kp){
    if(input == 'k') *pid_to_change = *pid_to_change + 0.0001;
    else if(input == 'j') *pid_to_change = *pid_to_change - 0.0001;
  }
  else{ // Change for PID constants except for kp
    if(input == 'k') *pid_to_change = *pid_to_change + 0.002;
    else if(input == 'j') *pid_to_change = *pid_to_change - 0.002;
    myPID.SetTunings(kp, ki, kd); 
  }
}

/* Print PID Values
 */
void print_pid(){
  Serial.print("Kp: ");
  Serial.println(kp);
  Serial.print("Ki: ");
  Serial.println(ki);
  Serial.print("Kd: ");
  Serial.println(kd);
}

void clear_string() {
  input_string = "";  
}

/* Zero Voltages
 *    - completely idles the system (motor will be loose, magnet off)
 */
void zero_voltages() {
  set_magnet_state(OFF);
  analogWrite(MOTOR_CW_PIN, 0);
  analogWrite(MOTOR_CCW_PIN, 0);
}

void brake() {
  analogWrite(MOTOR_CW_PIN, 255);
  analogWrite(MOTOR_CCW_PIN, 255);
}

void set_magnet_state(MagnetState magnetState) {
  if (magnetState == OFF) {
    magnet_state = OFF;
    analogWrite(MAGNET_PIN, 255); // Magnet is active low, so 255 = OFF
    digitalWrite(LED_PIN, LOW);
  }
  else if (magnetState == ON) {
    magnet_state = ON;
    analogWrite(MAGNET_PIN, 20);
    digitalWrite(LED_PIN, HIGH);
    timer.after(2000, magnet_off); // Add a callback to turn timer off after 2 seconds
  }
}

void magnet_off(){
  set_magnet_state(OFF);
}


/* Move To 
 *    - Takes a target position and drives motor with PID output
 */
void move_to(double target) {
  Input = double(read_encoder(0));  
  Setpoint = target;
  if (Input < target) {
    myPID.Compute();
    analogWrite(MOTOR_CCW_PIN, int(Output * 2.55));
    analogWrite(MOTOR_CW_PIN, 0);
  }
  else if (Input > target) {
    myPID.Compute();
    analogWrite(MOTOR_CW_PIN, -int(Output * 2.55));
    analogWrite(MOTOR_CCW_PIN, 0);
  }

  if(print_verbose){
    Serial.print("Encoder: ");
    Serial.print(Input);
    Serial.print("       Duty: ");
    Serial.print(Output);
    Serial.print("       Set: ");
    Serial.println(Setpoint);
  }
}

/* Hold position
 *    - by setting the PID to maintain the current position, hold at the position for a certain number of loops
 */
void hold_position(double target, uint16_t num_loops){
  for(uint16_t i = 0; i < num_loops; i++){
    move_to(target);
  }
}

bool target_reached(int32_t target) {
//  Serial.print("Err: ");
//  Serial.print(encoder_data.get_absolute_error(target));
//  Serial.print("PP: ");
//  Serial.println(encoder_data.get_peak_to_peak_ripple());
  return encoder_data.get_absolute_error(target) < ERROR_TOLERANCE && 
  encoder_data.get_peak_to_peak_ripple() < RIPPLE_TOLERANCE; 
}

/* Send state to GUI
 *    - encoder value, kp, ki, kd, state
 *    - setpoint, PID output
 */
void send_state(){
  Serial.print(int(read_encoder(0)));
  Serial.print(",");
  Serial.print(kp, 5);
  Serial.print(",");
  Serial.print(ki, 3);
  Serial.print(",");
  Serial.print(kd, 3);
  Serial.print(",");
  Serial.print(state); 
  Serial.print(",");
  Serial.print(int(Setpoint));
  Serial.print(",");
  Serial.print(Output);
  Serial.print(",");
  Serial.print(ERROR_TOLERANCE);
  Serial.print(",");
  Serial.print(RIPPLE_TOLERANCE);
  Serial.print(",");
  Serial.println(run_timer.get_delta());
}

/* Limit Switch Functions */
void limit_switch_1(){
  state = STATE_IDLE;
  #if MANUAL
    Serial.println("LIMIT_1");
  #endif
}

void limit_switch_2(){
  state = STATE_IDLE;
  #if MANUAL
    Serial.println("LIMIT_2");
  #endif
}

void limit_switch_3(){
  run_timer.stop_time();   // Internal debouncing since stop_time holds the delta value until reset is called
}


void add_sample(void){
  encoder_data.add_sample(read_encoder(0));
}



